﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace EasyJson
{
//    class MainClass
//    {
//        [Serialize]
//        class ObjectClass
//        {
//            int member0 = 0;
//            int member1 = 1;
//            int member2 = 2;
//            int[] member_array = { 2, 3, 2, 5, 9, 3, 6 };
//        }
//            
//        [Serialize]
//        class TestClass
//        {
//            [Serialize]
//            public struct testStruct
//            {
//                public int x;
//                public string name;
//            };
//
//            public enum Test
//            {
//                One,
//                Two,
//                Three
//            }
//
//            bool boolValue = false;
//
//            string stringValue = "hello world";
//            int intValue = 245;
//            long longValue = 2147483648;
//            double doubleValue = -15023452221990199574E-09;
//            ObjectClass objectClass = new ObjectClass();
//            IDictionary dictionary = new Dictionary<string, int> { ["width"] = 1920, ["height"] = 1080 };
//            IDictionary dictionary2 = new Dictionary<int, float>{ [0] = 1.15f, [1] = -2730E-4f };
//            string[] array = { "one", "two", "three", "four" };
//
//            testStruct xStruct = new testStruct() { x = 89, name = "name" };
//            public Test x = Test.Three;
//        }
//
//        public static JsonData IPEndPointExporter( object obj )
//        {
//            IPEndPoint endpoint = (IPEndPoint)obj;
//            JsonData data = new JsonData();
//            data.Add("IPAddress", endpoint.Address.ToString());
//            data.Add("Port", endpoint.Port);
//            return data;
//        }
//
//        public static IPEndPoint IPEndPointImporter( JsonData data )
//        {
//            return new IPEndPoint(IPAddress.Parse(data["IPAddress"].GetString() ), data["Port"].GetInt()); 
//        }
//
//        public static void Main(string[] args)
//        {
//            {
//                IPEndPoint clsExport = new IPEndPoint( IPAddress.Parse( "192.168.8.241" ), 7012 );
//
//                JsonData exportData = ObjectMapper.ToJsonData( clsExport, true );
//                string json = JsonMapper.ToJson(exportData);
//
//                Console.Write(json);
//
//                ObjectMapper.RegisterFactory<IPEndPoint>( () => { return new IPEndPoint( IPAddress.Parse( "0.0.0.0" ), 0 ); } );
//                ObjectMapper.RegisterFactory<IPAddress>( () => { return IPAddress.Parse( "0.0.0.0" ); } );
//
//                JsonData importData = JsonMapper.ToJsonData(json);
//                IPEndPoint clsImport = ObjectMapper.ToObject<IPEndPoint>(importData, true) as IPEndPoint;
//            }
//
//            Console.ReadKey();
//
//            {
//                TestClass clsExport = new TestClass();
//                JsonData exportData = ObjectMapper.ToJsonData( clsExport );
//                string json = JsonMapper.ToJson(exportData);
//
//                Console.Write(json);
//
//                JsonData importData = JsonMapper.ToJsonData(json);
//                TestClass clsImport = ObjectMapper.ToObject<TestClass>(importData) as TestClass;
//            }
//
//            Console.ReadKey();
//        }
//    
//

    class MyProfile : Profile
    {
        [ProfileKey]
        public string name;
        public int value;

        public MyProfile()
        {
        }
        
        public MyProfile( string name, int value )
        {
            this.name = name;
            this.value = value;
        }
    };

    public class MainClass
    {
        public static void Main(string[] args)
        {
            ProfileManager<MyProfile> manager = ProfileManager<MyProfile>.LoadProfile("testProfile.json");

//            manager.AddProfile(new MyProfile("1", 1));
//            manager.AddProfile(new MyProfile("2", 2));
//            manager.AddProfile(new MyProfile("3", 3));

            MyProfile profile = manager.GetProfile("3");
            profile.value = 256;

            ProfileManager<MyProfile>.SaveProfile(manager);

            Console.ReadKey();
        }
    }
}
